import { StyleSheet, Text, View, ScrollView } from "react-native";
import React from "react";
import { useRouter } from "expo-router";

import { useSelector } from "react-redux";
import { RootState } from "../../../types/reduxState/ReduxState";
import { themeGlobal } from "../../../styles/themeGlobal";
import ProductProduct from "../../../components/productproduct/ProductProduct";

const promotion = () => {
  const products = useSelector((state: RootState) => state.product.products);
  const router = useRouter();

  return (
    <View style={themeGlobal.baseStyles.container}>
      <View style={styles.title}>
        <Text style={styles.recommendedforyou}>{`Recommended for you`}</Text>
      </View>
      <ScrollView>
        <View
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
          }}
        >
          {products.map &&
            products.map((product, index: number) => (
              <View key={index}>
                <ProductProduct
                  orientation="Horizontal"
                  title={product.title ?? ""}
                  image={product.image ?? ""}
                  price={product.price ?? ""}
                  promotion={product.promotion ?? ""}
                />
              </View>
            ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default promotion;

const styles = StyleSheet.create({
  title: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
  },
  recommendedforyou: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24,
  },
});
