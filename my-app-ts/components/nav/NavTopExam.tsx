import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Svg, Path, Circle } from "react-native-svg";

export default function Navtop() {
  return (
    <View style={styles.navtop}>
      {/* Vigma RN:: can be replaced with <IconsOutlineArrow_back  /> */}
      <View style={styles.iconsOutlineArrow_back}>
        <View style={styles.boundingbox} />
        <Svg
          style={styles.vector}
          width="16"
          height="16"
          viewBox="0 0 16 16"
          fill="none"
        >
          <Path
            d="M6.875 15.3L0.275 8.69999C0.175 8.59999 0.104167 8.49165 0.0625 8.37499C0.0208333 8.25832 0 8.13332 0 7.99999C0 7.86665 0.0208333 7.74165 0.0625 7.62499C0.104167 7.50832 0.175 7.39999 0.275 7.29999L6.875 0.699987C7.05833 0.516654 7.2875 0.420821 7.5625 0.412487C7.8375 0.404154 8.075 0.499987 8.275 0.699987C8.475 0.883321 8.57917 1.11249 8.5875 1.38749C8.59583 1.66249 8.5 1.89999 8.3 2.09999L3.4 6.99999H14.575C14.8583 6.99999 15.0958 7.09582 15.2875 7.28749C15.4792 7.47915 15.575 7.71665 15.575 7.99999C15.575 8.28332 15.4792 8.52082 15.2875 8.71249C15.0958 8.90415 14.8583 8.99999 14.575 8.99999H3.4L8.3 13.9C8.48333 14.0833 8.57917 14.3167 8.5875 14.6C8.59583 14.8833 8.5 15.1167 8.3 15.3C8.11667 15.5 7.88333 15.6 7.6 15.6C7.31667 15.6 7.075 15.5 6.875 15.3Z"
            fill="#09111F"
          />
        </Svg>
      </View>
      <View style={styles.frame26080030}>
        {/* Vigma RN:: can be replaced with <Notification state={"active"} /> */}
        <View style={styles.notification}>
          {/* Vigma RN:: can be replaced with <Iconfilled  /> */}
          <View style={styles.iconfilled}>
            <Svg
              style={styles.vectorStroke}
              width="16"
              height="16"
              viewBox="0 0 16 16"
              fill="none"
            >
              <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M3.41667 2.16685C2.72631 2.16685 2.16667 2.72649 2.16667 3.41685C2.16667 4.10721 2.72631 4.66685 3.41667 4.66685C4.10702 4.66685 4.66667 4.10721 4.66667 3.41685C4.66667 2.72649 4.10702 2.16685 3.41667 2.16685ZM0.5 3.41685C0.5 1.80602 1.80584 0.500183 3.41667 0.500183C5.0275 0.500183 6.33333 1.80602 6.33333 3.41685C6.33333 5.02768 5.0275 6.33352 3.41667 6.33352C1.80584 6.33352 0.5 5.02768 0.5 3.41685ZM13.2441 1.57759C13.5695 1.25216 14.0972 1.25216 14.4226 1.57759C14.748 1.90303 14.748 2.43067 14.4226 2.75611L2.75592 14.4228C2.43049 14.7482 1.90285 14.7482 1.57741 14.4228C1.25197 14.0973 1.25197 13.5697 1.57741 13.2443L13.2441 1.57759ZM12.5833 11.3335C11.893 11.3335 11.3333 11.8932 11.3333 12.5835C11.3333 13.2739 11.893 13.8335 12.5833 13.8335C13.2737 13.8335 13.8333 13.2739 13.8333 12.5835C13.8333 11.8932 13.2737 11.3335 12.5833 11.3335ZM9.66667 12.5835C9.66667 10.9727 10.9725 9.66685 12.5833 9.66685C14.1942 9.66685 15.5 10.9727 15.5 12.5835C15.5 14.1943 14.1942 15.5002 12.5833 15.5002C10.9725 15.5002 9.66667 14.1943 9.66667 12.5835Z"
                fill="black"
              />
            </Svg>
          </View>
          <Text style={styles.label}>{`Promo`}</Text>
        </View>
        {/* Vigma RN:: can be replaced with <Itemright state={"default"} /> */}
        <View style={styles.itemright}>
          {/* Vigma RN:: can be replaced with <IconsOutlineNotifications  /> */}
          <View style={styles.iconsOutlineNotifications}>
            <View style={styles._boundingbox} />
            <Svg
              style={styles._vector}
              width="14"
              height="18"
              viewBox="0 0 14 18"
              fill="none"
            >
              <Path
                d="M1.16683 14.8334C0.930718 14.8334 0.732802 14.7535 0.573079 14.5938C0.413357 14.434 0.333496 14.2361 0.333496 14C0.333496 13.7639 0.413357 13.566 0.573079 13.4063C0.732802 13.2465 0.930718 13.1667 1.16683 13.1667H2.00016V7.33335C2.00016 6.18058 2.34738 5.15627 3.04183 4.26044C3.73627 3.3646 4.63905 2.7778 5.75016 2.50002V1.91669C5.75016 1.56946 5.87169 1.27433 6.11475 1.03127C6.3578 0.788215 6.65294 0.666687 7.00016 0.666687C7.34738 0.666687 7.64252 0.788215 7.88558 1.03127C8.12863 1.27433 8.25016 1.56946 8.25016 1.91669V2.50002C9.36127 2.7778 10.2641 3.3646 10.9585 4.26044C11.6529 5.15627 12.0002 6.18058 12.0002 7.33335V13.1667H12.8335C13.0696 13.1667 13.2675 13.2465 13.4272 13.4063C13.587 13.566 13.6668 13.7639 13.6668 14C13.6668 14.2361 13.587 14.434 13.4272 14.5938C13.2675 14.7535 13.0696 14.8334 12.8335 14.8334H1.16683ZM7.00016 17.3334C6.54183 17.3334 6.14947 17.1702 5.82308 16.8438C5.49669 16.5174 5.3335 16.125 5.3335 15.6667H8.66683C8.66683 16.125 8.50364 16.5174 8.17725 16.8438C7.85086 17.1702 7.4585 17.3334 7.00016 17.3334ZM3.66683 13.1667H10.3335V7.33335C10.3335 6.41669 10.0071 5.63197 9.35433 4.97919C8.70155 4.32641 7.91683 4.00002 7.00016 4.00002C6.0835 4.00002 5.29877 4.32641 4.646 4.97919C3.99322 5.63197 3.66683 6.41669 3.66683 7.33335V13.1667Z"
                fill="#09111F"
              />
            </Svg>
          </View>
          {/* Vigma RN:: can be replaced with <Badge type={"dot"} size={"small"} /> */}
          <View style={styles.badge}>
            <Svg
              style={styles.dot}
              width="6"
              height="6"
              viewBox="0 0 6 6"
              fill="none"
            >
              <Circle cx="3" cy="3" r="3" fill="#E50D24" />
            </Svg>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  navtop: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingLeft: 24,
    paddingRight: 14,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 0,
    paddingVertical: 12,
  },
  iconsOutlineArrow_back: {
    flexShrink: 0,
    height: 24,
    width: 24,
    alignItems: "flex-start",
    rowGap: 0,
  },
  boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  vector: {
    position: "absolute",
    flexShrink: 0,
    top: 4,
    right: 4,
    bottom: 4,
    left: 4,
    overflow: "visible",
  },
  frame26080030: {
    flexShrink: 0,
    width: 123,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 10,
  },
  notification: {
    flexShrink: 0,
    paddingLeft: 10,
    paddingRight: 14,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    paddingVertical: 10,
    borderRadius: 9999,
  },
  iconfilled: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  vectorStroke: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 2,
    bottom: 2,
    left: 3,
    overflow: "visible",
  },
  label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20,
  },
  itemright: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999,
  },
  iconsOutlineNotifications: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 2,
    left: 3,
    overflow: "visible",
  },
  badge: {
    position: "absolute",
    flexShrink: 0,
    top: 7,
    height: 6,
    left: 27,
    width: 6,
    alignItems: "flex-start",
    rowGap: 0,
  },
  dot: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: "visible",
  },
});
