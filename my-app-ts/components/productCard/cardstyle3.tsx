import React from "react";
import { ImageBackground, StyleSheet } from "react-native";
import {} from "react-native-svg";

export default function Image() {
  return (
    <ImageBackground
      style={styles.image}
      source={{ uri: "https://dummyimage.com/312x125/000/fff.jpg" }}
    />
  );
}

const styles = StyleSheet.create({
  image: {
    flexShrink: 0,
    width: 312,
    height: 125,
    borderRadius: 10,
  },
});
