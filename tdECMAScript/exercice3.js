// TODO : Utilisez les littéraux d'objet améliorés pour créer un objet "utilisateur" avec des
//propriétés telles que le nom, l'adresse e-mail, etc.
// N'oubliez pas d'utiliser la nouvelle syntaxe d'objet améliorée.
const nom = "Alice";
const email = "alice@email.com";
const age = 30;
// TODO : Créez un objet "utilisateur" en utilisant les littéraux d'objet améliorés

const utilisateur = {
// TODO : Utilisez la nouvelle syntaxe pour définir les propriétés
    nom,
    email,
    age
};
const presenter = (utilisateur) => {
    console.log(`La personne est de nom : ${utilisateur.nom} a un email : ${utilisateur.email} et  d'age : ${utilisateur.age} ans.`);
}
// Test de l'objet utilisateur
console.log(utilisateur); // Attendu : { nom: 'Alice', email: 'alice@email.com', age: 30 }