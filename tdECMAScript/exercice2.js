// TODO : Créez une classe "Personne" avec des propriétés telles que le nom, l'âge, etc. Ajoutez 
// ensuite des méthodes pour obtenir et définir ces propriétés.
class Personne {
    constructor(nom, age) {
    // TODO
        this.nom = nom;
        this.age = age;
    }

    
// TODO : Ajoutez des méthodes pour obtenir et définir les propriétés

    getAge() {
        return this.age;
    }

    setAge(age) {
        this.age = age;
    }

    getNom() {
        return this.nom;
    }

    setNom(nom) {
        this.nom = nom;
    }

    presenter() {
        console.log(`La personne est de nom : ${this.nom} et  d'age : ${this.age} ans.`);
    }

}
const personne1 = new Personne("Alice", 25);
// TODO : Testez les méthodes de la classe
personne1.presenter();